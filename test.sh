CORRECT=0
WRONG=0
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
PROG=OptimalBuild
for f in /home/students/inf/m/mp406304/PO/optimal-build/testy/*.in; do

	OUTF="$(mktemp)";
    echo "Saving temporary results to $OUTF";
    echo -n "Testing $f ...";
	java $PROG < $f 1>$OUTF.out 2>$OUTF.err

	
	LAST_LINE=$( tail -1 $f )
	if [ $LAST_LINE == "ekonomiczna" ]
	then
			diff <(head -1 ${f%in}out) <(head -1  $OUTF.out) &>/dev/null
	else
		if [ $LAST_LINE == "ekologiczna" ]
		then
			diff <(head -2 ${f%in}out | tail -1) <(head -2  $OUTF.out | tail -1) &>/dev/null
		else
			diff <(head -2 ${f%in}out) <(head -2  $OUTF.out) &>/dev/null
		fi
	fi
	OUT_DIFF=$?
	if [ $OUT_DIFF -eq 0 ];
	then
		echo -e " ${GREEN}OK${NC}";
		((CORRECT+=1));
	else
		if [ $OUT_DIFF -ne 0 ];
		then
		    echo ""
		    echo -e "-------${RED}wrong output${NC}";
		    echo "----------Diff in .out = $OUT_DIFF"
		    echo -e "${GREEN}"
		    cat ${f%in}out
		    echo -e "${NC} vs"
		    echo -e "${RED}"
			cat $OUTF.out
		    echo -e "${NC}"

		fi
		((WRONG+=1));
	fi
	echo ""
	echo ""
	rm -f $OUTF.out $OUTF $OUTF.err
done;

echo -e "Passed ${GREEN}$CORRECT${NC} tests, failed ${RED}$WRONG${NC} tests."
