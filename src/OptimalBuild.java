import com.procyk.build.project.PriceList;
import com.procyk.build.project.RodsCollection;
import com.procyk.build.strategy.SelectRodStrategy;
import com.procyk.build.strategy.StrategyBuilder;
import com.procyk.build.tools.InputManager;

/**
 * The main class of the project which loads data from
 * standard input, creates a price list of rods and
 * build project and uses it to find te most optimal
 * solution for given in data strategy
 */
public class OptimalBuild {

    public static void main(String... args) {

        InputManager dataInput = new InputManager();

        PriceList priceList = new PriceList();
        RodsCollection projectRods = new RodsCollection();
        StrategyBuilder strategyBuilder = new StrategyBuilder();

        // loadData function loads data to priceList and projectRods
        // and returns the strategy read from input stream as a String value
        SelectRodStrategy strategy = strategyBuilder.getSelectRodStrategy(dataInput.loadData(priceList, projectRods, System.in));

        // try to find a solution for given strategy problem
        // when not possible throws an IllegalArgumentException
        // so we can detect the reason of no possible ways to
        // realize the project
        try {
            strategy.findSolution(projectRods, priceList);
        }
        catch (IllegalArgumentException exception) {
            System.out.println("No possible way to buy project rods from shop");
            System.out.println(exception.getMessage());
        }

        // print the solution only if exists as stated in
        if (strategy.hasSolution()) {
            System.out.println(strategy);
        }
    }
}
