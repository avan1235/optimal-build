package com.procyk.build.tools;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Class used InputManager for specific input for optimal build project
 * in order to get the input data in characteristic format.
 * Implemented using Scanner object for reading from given in
 * parameter inputStream stream (so can be set for file reading)
 */
public class InputManager {

    /**
     * Method to load the project data as described
     * in the task description format
     * @param priceList     the price list in which the shop prices are added
     * @param projectRods   collections of rods to which rods are added
     * @param inputStream   stream to load data from
     * @return              strategy name read from inputStream
     */
    public String loadData(PriceList priceList, RodsCollection projectRods, InputStream inputStream) {

        Scanner input = new Scanner(inputStream);

        int priceListLength = input.nextInt();

        while (priceListLength > 0){
            priceList.addRodWithPrice(new Rod(input.nextInt()), input.nextInt());
            priceListLength--;
        }

        int projectLength = input.nextInt();

        while (projectLength > 0){
            projectRods.addRod(new Rod(input.nextInt()));
            projectLength--;
        }

        String strategyName = "";

        while (strategyName.length() == 0) {
            strategyName = input.nextLine();
            strategyName = strategyName.replaceAll("\\s+","");
        }

        return strategyName;
    }
}
