package com.procyk.build.tools;

/**
 * Functional interface defined to use the lambda expressions
 * for the function that takes 2 arguments and returns one
 * but also can throw an exception which can be caught in
 * special cases
 * @param <T>       first parameter of function
 * @param <U>       second parameter of function
 * @param <R>       value returned by function
 */

@FunctionalInterface
public interface BiFunctionThrows<T, U, R> {
    R apply(T t, U u) throws Exception;
}
