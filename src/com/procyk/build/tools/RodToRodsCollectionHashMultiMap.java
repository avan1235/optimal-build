package com.procyk.build.tools;

import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Hashed multiMap to represent the connection
 * between one rod from price list and many possible
 * it parts cuttings
 */
public class RodToRodsCollectionHashMultiMap {
    protected final HashMap<Rod, ArrayList<RodsCollection>> mapEntries;

    public RodToRodsCollectionHashMultiMap() {
        mapEntries = new HashMap<>();
    }

    /**
     * Add next pair to the hash map by checking if exists
     * the given key and if not the create one
     * @param r     rod key
     * @param c     rods collection to connect with key
     */
    public void addPair(Rod r, RodsCollection c) {
        if (mapEntries.containsKey(r)) {
            mapEntries.get(r).add(c);
        }
        else {
            ArrayList<RodsCollection> collectionsList = new ArrayList<>();
            collectionsList.add(c);
            mapEntries.put(r, collectionsList);
        }
    }
}
