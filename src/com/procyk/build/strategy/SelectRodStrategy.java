package com.procyk.build.strategy;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.RodsCollection;
import com.procyk.build.tools.RodToRodsCollectionHashMultiMap;

/**
 * Class for finding the solution of optimal build by
 * creating the concrete strategy which will eb able to
 * update the fields of strategy solution and check if
 * found solution
 */
public abstract class SelectRodStrategy {

    protected boolean hasSolution;
    protected final ConstructionProblemSolution problemSolution;

    public SelectRodStrategy() {
        hasSolution = false;
        problemSolution = new ConstructionProblemSolution();
    }

    /**
     * Find solution for specified project and the price list
     * @param projectRodsCollection     project specification
     * @param priceList                 price list for project
     */
    public abstract void findSolution(RodsCollection projectRodsCollection, PriceList priceList);

    /**
     * Check if strategy has found the solution
     * @return      true when has solution otherwise false
     */
    public boolean hasSolution() {
        return hasSolution;
    }

    /**
     * Print the strategy solution in specified format using the description
     * of problem solution
     * @return      string describing the strategy result
     */
    public String toString() {
        return problemSolution.getDescription();
    }

    /**
     * ConstructionProblemSolution class to represent the actual problemSolution
     * for optimal construction problem solving by findSolution method
     * Keeps also all the pairs of rods and their parts in hashMultiMap
     */
    protected class ConstructionProblemSolution extends RodToRodsCollectionHashMultiMap {
        private long priceSummary;
        private long wasteSummary;

        protected void increasePriceSummaryBy(long price) {
            this.priceSummary += price;
        }

        protected void increaseWasteSummary(long waste) {
            this.wasteSummary += waste;
        }

        private String getDescription() {
            final StringBuilder out = new StringBuilder();
            out.append(priceSummary);
            out.append(System.lineSeparator());
            out.append(wasteSummary);

            mapEntries.forEach(
                    (rod, collectionArrayList) -> {
                        for (RodsCollection collection : collectionArrayList) {
                            out.append(System.lineSeparator());
                            out.append(rod.getLength());

                            for (int i = 0; i < collection.size(); i++) {
                                out.append(" ");
                                out.append(collection.getRod(i).getLength());
                            }
                        }
                    }
            );

            return out.toString();
        }
    }
}
