package com.procyk.build.strategy;

/**
 * Strategy builder class for creating the objects of
 * strategies based on the given string
 */
public class StrategyBuilder {

    public SelectRodStrategy getSelectRodStrategy(String strategyName) {

        if (strategyName.equalsIgnoreCase("minimalistyczna")) {
            return new MinimalistStrategy();
        }
        else if (strategyName.equalsIgnoreCase("maksymalistyczna")) {
            return new MaximalistStrategy();
        }
        else if (strategyName.equalsIgnoreCase("ekologiczna")) {
            return new EcologicalStrategy();
        }
        else if (strategyName.equalsIgnoreCase("ekonomiczna")) {
            return new EconomicalStrategy();
        }
        else {
            throw new IllegalArgumentException("Bad parameter as strategy name");
        }
    }
}
