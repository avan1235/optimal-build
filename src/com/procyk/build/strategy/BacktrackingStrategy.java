package com.procyk.build.strategy;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

import java.util.List;
import java.util.Objects;

/**
 * Backtracking strategy which generates all possible partitions
 * of the set of the project rods and the try to fit
 * the rods from the price list to best fit the given optimization conditions
 */
public abstract class BacktrackingStrategy extends SelectRodStrategy{

    /**
     * Selection from price list best rod for single rods collection
     * based on the specified strategy which uses backtracking method
     * @param collection        single set of rods to be fitted to rod from price list
     * @param priceList         possible to buy rods
     * @return                  best fitted conditions rod
     */
    public abstract Rod selectOptimizedRodFromPriceList(RodsCollection collection, PriceList priceList);

    /**
     * For calculating statistics of fitting rods problem we have to use
     * the selected value to be optimized so we have to calculate it
     * based on best solution rod
     * @param optimizedRodFromPriceList chosen rod from price list in function
     *                                  selectOptimizedRodFromPriceList
     * @param collection                single collection of rods
     * @param priceList                 price list for prices of rods which may be used
     * @return                          increased by value
     */
    public abstract long increaseOptimizedValueBy(Rod optimizedRodFromPriceList, RodsCollection collection, PriceList priceList);

    /**
     * Function to get the ability in child classes to optimize the searching for
     * the best fitting of rods by throwing an Exception which means that
     * there has been found best fitting and we don't have to generate next
     * fitting because the generated one is the best one
     * @param optimizedValue            value by which we can decide if fitting is
     *                                  one of the best possible
     * @throws Exception                thrown when optimized value is best
     *                                  one for given strategy
     */
    public abstract void checkForOptimizations(long optimizedValue) throws Exception;

    /**
     * Optimization function which search for best solution in
     * generated all possible project rods set partitions using the
     * abstract function from child classes
     * @param partitionSet          generated partition
     * @param priceList             current price list
     * @return                      best optimized value to be compared
     *                              in order to find the best one of them
     *                              Long.MAX_VALUE when catch exception
     * @throws Exception            when best value found and we can
     *                              say that it is surely one of best
     *                              fittings
     */
    private Long optimizeValue(List<RodsCollection> partitionSet, PriceList priceList) throws Exception {
        long optimizedValue = 0;

        for (RodsCollection set : partitionSet) {
            Rod optimizedRodFromPriceList;
            try {
                optimizedRodFromPriceList = selectOptimizedRodFromPriceList(set, priceList);
            } catch (IllegalArgumentException exception) {
                optimizedValue = Long.MAX_VALUE;
                break;
            }

            optimizedValue += increaseOptimizedValueBy(optimizedRodFromPriceList, set, priceList);
        }

        checkForOptimizations(optimizedValue);

        return optimizedValue;
    }

    /**
     * Solution finding implementation which changes the problemSolution
     * after finding best optimized set of rods from the price list
     * @param projectRodsCollection     project rods
     * @param priceList                 price list with possible to buy rods
     */
    @Override
    public void findSolution(RodsCollection projectRodsCollection, PriceList priceList) {

        RodsPartitionsGenerator generator = new RodsPartitionsGenerator();

        List<RodsCollection> bestSet = generator.generateOptimizedSet(projectRodsCollection, priceList, this::optimizeValue);

        if (Objects.isNull(bestSet)) {
            throw new IllegalArgumentException("Cannot find best solution using backtracking strategy for this arguments");
        }

        for (RodsCollection partOfProject : bestSet) {
            Rod optimizedRod = selectOptimizedRodFromPriceList(partOfProject, priceList);
            problemSolution.increasePriceSummaryBy(priceList.getPriceOf(optimizedRod));
            problemSolution.increaseWasteSummary(optimizedRod.getLength() - partOfProject.getSummaryLengthOfRods());
            problemSolution.addPair(optimizedRod, partOfProject);
        }

        hasSolution = true;
    }
}
