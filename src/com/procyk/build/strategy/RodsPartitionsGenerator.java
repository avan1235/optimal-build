package com.procyk.build.strategy;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.RodsCollection;
import com.procyk.build.tools.BiFunctionThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Class for finding all possible partitions of set
 * and translating them into all possible rods collections
 * and using them to optimize same value to min
 * using the optimizeFunction
 */
public class RodsPartitionsGenerator {

    /**
     * Generate optimized set of rods collection and return it as a list
     * using the optimize function to optimize some value for generated
     * partition of given project rods and find best fitting
     * @param projectRodsCollection     project rods to be partitioned
     * @param priceList                 price list for calculating optimized value
     * @param optimizeFunction          function to optimize minimized value
     * @return                          best optimized set of rods collections
     */
    public List<RodsCollection> generateOptimizedSet(RodsCollection projectRodsCollection, PriceList priceList, BiFunctionThrows<List<RodsCollection>, PriceList, Long> optimizeFunction) {
        int n = projectRodsCollection.size(), m = 1, j;
        int[] positions = new int[n];
        int[] maxes = new int[n];

        for (int i = 0; i < n; i++) {
            positions[i] = 0;
            maxes[i] = 1;
        }

        List<RodsCollection> bestOptimizedSets = null;
        long minimizedValue = Long.MAX_VALUE;
        long currentValue;

        // implemented algorithm for finding all possible
        // set partitions and using them for optimizations
        generatingLoop:
        while (true) {
            while (true) {
                List<RodsCollection> currentSets = generateNewCaseSets(positions, projectRodsCollection);

                try {
                    currentValue = optimizeFunction.apply(currentSets, priceList);
                } catch (Exception e) {
                    // optimizeFunction thrown exception so the function
                    // which optimize the value decided that the current
                    // fitting is the best one and there cannot be better
                    // fitting so we can break our searching
                    bestOptimizedSets = currentSets;
                    break generatingLoop;
                }

                if (currentValue <= minimizedValue) {
                    minimizedValue = currentValue;
                    bestOptimizedSets = currentSets;
                }

                if (positions[n-1] == m) break;
                else positions[n-1] += 1;
            }
            j = n - 1;
            while (positions[j-1] == maxes[j-1]) {
                j -= 1;
            }
            if (j == 1) break;
            else positions[j-1] += 1;

            m = maxes[j-1] + (positions[j-1] == maxes[j-1] ? 1 : 0);
            j += 1;
            while (j < n) {
                positions[j-1] = 0;
                maxes[j-1] = m;
                j += 1;
            }
            positions[n-1] = 0;
        }

        return bestOptimizedSets;
    }

    /**
     * Generate the List of RodsCollections which is the partition
     * of the project collection rod based o the indexes array
     * @param positions                 indexes array
     * @param projectRodsCollection     project rods
     * @return                          list of partitions of project
     */
    private static List<RodsCollection> generateNewCaseSets(int[] positions, RodsCollection projectRodsCollection) {
        int howManySets = Integer.MIN_VALUE;

        for (int p : positions) {
            howManySets = (howManySets < p ? p : howManySets);
        }
        howManySets += 1;

        List<RodsCollection> newPartition = new ArrayList<>(howManySets);

        for (int i = 0; i < howManySets; i++) {
            newPartition.add(new RodsCollection(positions.length - howManySets + 1));
        }

        for (int i = 0; i < positions.length; i++) {
            newPartition.get(positions[i]).addRod(projectRodsCollection.getRod(i));
        }

        return newPartition;
    }
}
