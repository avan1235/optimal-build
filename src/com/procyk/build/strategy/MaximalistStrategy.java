package com.procyk.build.strategy;

import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

public class MaximalistStrategy extends GreedyStrategy {

    /**
     * Get the Longest rod from price list rods collection
     * and check if it is long enough
     * @param rod               next longest rod from project
     * @param rodsCollection    rods from price list sorted
     * @return                  selected rod
     */
    @Override
    public Rod chooseRodToBuy(Rod rod, RodsCollection rodsCollection) {
        Rod newRod = RodsCollection.getLongestRodFromCollection(rodsCollection);

        if (newRod.getLength() < rod.getLength()) {
            throw new IllegalArgumentException(
                    "There is no long enough rod in collection to satisfy the rod of length " + rod.getLength());
        }
        else {
            return newRod;
        }
    }
}
