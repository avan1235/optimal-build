package com.procyk.build.strategy;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

/**
 * Greedy strategy implementation class which uses the
 * specified in child class chooseRodToBuy strategy
 * and finds some solution for given price list and project
 */
public abstract class GreedyStrategy extends SelectRodStrategy {

    /**
     * Choose from current price list rods collection method to
     * select the optimized value in greedy strategy
     * @param rod               next longest rod from project
     * @param rodsCollection    rods from price list sorted
     * @return                  chosen rod
     */
    public abstract Rod chooseRodToBuy(Rod rod, RodsCollection rodsCollection );

    /**
     * Finding solution function to implement the greedy strategies by
     * selecting the longest rod from the project and pairing it with some
     * other rods based on the selected from price list rod
     * @param projectRodsCollection     project rods collection
     * @param priceList                 project price list
     */
    @Override
    public void findSolution(RodsCollection projectRodsCollection, PriceList priceList) {

        projectRodsCollection.sortByDecreasingRodsLength();

        RodsCollection availableRodsSorted = priceList.getAvailableRodsSortedIncreasing();

        Rod currentBuyRod;
        Rod wasteRod;
        RodsCollection rodsWhichFitsInCurrentBuyRod;

        while (projectRodsCollection.size() > 0) {
            // we assume that there is always long enough rod in available rods
            // collection in which every rod from collection will fit so we don't
            // have to catch the IllegalArgumentException from method.
            // In more general implementation hasSolution field in
            // SelectRodStrategy should be changed to false when the exception
            // is thrown
            currentBuyRod = chooseRodToBuy(projectRodsCollection.getRod(0), availableRodsSorted);

            rodsWhichFitsInCurrentBuyRod = new RodsCollection();
            rodsWhichFitsInCurrentBuyRod.addRod(new Rod(projectRodsCollection.getRod(0).getLength()));

            wasteRod = new Rod(currentBuyRod.getLength() - projectRodsCollection.getRod(0).getLength());
            projectRodsCollection.removeRod(0);

            // rods in projectRodsCollection are sorted in decreasing
            // length order so we start searching from the
            for (int i = 0; i < projectRodsCollection.size() && wasteRod.getLength() > 0; i++) {
                if (projectRodsCollection.getRod(i).getLength() <= wasteRod.getLength()) {
                    rodsWhichFitsInCurrentBuyRod.addRod(wasteRod.cut(projectRodsCollection.getRod(i).getLength()));
                    projectRodsCollection.removeRod(i);
                    i--; // to getRod of the shifted elements in rods collection
                }
            }

            problemSolution.increasePriceSummaryBy(priceList.getPriceOf(currentBuyRod));
            problemSolution.increaseWasteSummary(wasteRod.getLength());

            problemSolution.addPair(currentBuyRod, rodsWhichFitsInCurrentBuyRod);
        }

        hasSolution = true;
    }
}
