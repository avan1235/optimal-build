package com.procyk.build.strategy;

import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

public class MinimalistStrategy extends GreedyStrategy {

    /**
     * Get the shortest rod from price list collection
     * in which will fit in the currently considered rod
     * @param rod               next longest rod from project
     * @param rodsCollection    rods from price list sorted
     * @return                  selected rod
     */
    @Override
    public Rod chooseRodToBuy(Rod rod, RodsCollection rodsCollection ) {
        return RodsCollection.getShortestRodFromRodCollectionInWhichFitsRod(rod, rodsCollection);
    }
}
