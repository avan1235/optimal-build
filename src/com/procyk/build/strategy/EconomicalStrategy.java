package com.procyk.build.strategy;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

import java.util.ArrayList;
import java.util.Collections;

public class EconomicalStrategy extends BacktrackingStrategy {

    /**
     * Select the cheapest rod in which will fit given collection
     * @param collection        single set of rods to be fitted to rod from price list
     * @param priceList         possible to buy rods
     * @return                  best optimized rod
     */
    @Override
    public Rod selectOptimizedRodFromPriceList(RodsCollection collection, PriceList priceList) {

        long[] possibleToBuyLength = priceList.toArrayOfPriceListRodsLengths();
        ArrayList<PricePair> pricesAndLengths = new ArrayList<>();

        for (long length : possibleToBuyLength) {
            pricesAndLengths.add(new PricePair(length, priceList.getPriceOf(length)));
        }

        Collections.sort(pricesAndLengths);

        for (PricePair pair : pricesAndLengths) {
            if (pair.length >= collection.getSummaryLengthOfRods()) {
                return new Rod(pair.length);
            }
        }

        throw new IllegalArgumentException("Cannot fit rod from price list to project");
    }

    /**
     * Inrease the optimized value by the price of selected rod
     * @param optimizedRodFromPriceList chosen rod from price list in function
     *                                  selectOptimizedRodFromPriceList
     * @param collection                single collection of rods
     * @param priceList                 price list for prices of rods which may be used
     * @return                          price of selected rod
     */
    @Override
    public long increaseOptimizedValueBy(Rod optimizedRodFromPriceList, RodsCollection collection, PriceList priceList) {
        return priceList.getPriceOf(optimizedRodFromPriceList);
    }

    /**
     * Simple class to represent the pair of rods and theirs
     * prices from shop and sort them by price
     */
    private class PricePair implements Comparable<PricePair> {
        private final long length;
        private final long price;

        private PricePair(long length, long price) {
            this.length = length;
            this.price = price;
        }

        @Override
        public int compareTo(PricePair pricePair) {
            return Long.compare(this.price, pricePair.price);
        }
    }

    /**
     * In the case of economical strategy we have to check all possible
     * rods collection partitions so there are no optimizations
     * @param optimizedValue            value by which we can decide if fitting is
     *                                  one of the best possible
     */
    @Override
    public void checkForOptimizations(long optimizedValue) {}
}
