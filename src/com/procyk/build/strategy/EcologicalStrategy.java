package com.procyk.build.strategy;

import com.procyk.build.project.PriceList;
import com.procyk.build.project.Rod;
import com.procyk.build.project.RodsCollection;

import java.util.Arrays;

/**
 * Backtracking strategy which optimize the wastes from rods cutting
 */
public class EcologicalStrategy extends BacktrackingStrategy {

    /**
     * Selection of rod to get the smallest value of wastes
     * @param collection        single set of rods to be fitted to rod from price list
     * @param priceList         possible to buy rods
     * @return                  best optimized rod
     */
    @Override
    public Rod selectOptimizedRodFromPriceList(RodsCollection collection, PriceList priceList) {
        long[] possibleToBuyLengths = priceList.toArrayOfPriceListRodsLengths();
        Arrays.sort(possibleToBuyLengths);

        for (long possibleToBuyLength : possibleToBuyLengths) {
            if (possibleToBuyLength >= collection.getSummaryLengthOfRods()) {
                return new Rod(possibleToBuyLength);
            }
        }

        throw new IllegalArgumentException("Cannot fit rod from price list to project");
    }

    /**
     * Increase optimized value by rods cutting wastes
     * @param optimizedRodFromPriceList chosen rod from price list in function
     *                                  selectOptimizedRodFromPriceList
     * @param collection                single collection of rods
     * @param priceList                 price list for prices of rods which may be used
     * @return                          wastes from current rod
     */
    @Override
    public long increaseOptimizedValueBy(Rod optimizedRodFromPriceList, RodsCollection collection, PriceList priceList) {
        return optimizedRodFromPriceList.getLength() - collection.getSummaryLengthOfRods();
    }

    /**
     * The optimization in case of ecological strategy is then when
     * there are no wasted parts of rods so we can be sure that
     * current fitting is one of the best ones
     * @param optimizedValue            value by which we can decide if fitting is
     *                                  one of the best possible
     * @throws Exception                when found best fitting
     */
    @Override
    public void checkForOptimizations(long optimizedValue) throws Exception{
        if (optimizedValue == 0) {
            throw new Exception("0 wasted rods. Best solution found.");
        }
    }
}
