package com.procyk.build.project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * RodsCollection class to get the easily to illustrated
 * collection of rods on with special methods given
 * for its user and keeping the summary rods length
 */
public class RodsCollection {
    private final ArrayList<Rod> rods;
    private long summaryLength;

    /**
     * Basic constructors like in ArrayList class
     */
    public RodsCollection() {
        rods = new ArrayList<>();
        summaryLength = 0;
    }

    public RodsCollection(int size) {
        rods = new ArrayList<>(size);
        summaryLength = 0;
    }

    public RodsCollection(List<Rod> rods) {
        this.rods = new ArrayList<>(rods);
    }

    /**
     * Get the rod from collection at specified index
     * @param index     of rod to get
     * @return          rod from collection at given index
     */
    public Rod getRod(int index) {
        return rods.get(index);
    }

    /**
     * Removes the rod at specified index from collection
     * and returns it as a result of remove function
     * @param index     of rod to be removed
     * @return          removed rod
     */
    public Rod removeRod(int index) {
        summaryLength -= rods.get(index).getLength();
        return rods.remove(index);
    }

    /**
     * Add new rod ro collection
     * @param r     rod to be added
     */
    public void addRod(Rod r) {
        rods.add(r);
        summaryLength += r.getLength();
    }

    /**
     * Get the current size of rods collection
     * @return      size of collection
     */
    public int size() {
        return rods.size();
    }

    /**
     * Get summary length of rods in collection
     * @return       summary length of rods in collection
     */
    public long getSummaryLengthOfRods() {
        return summaryLength;
    }


    /**
     * Sort the collection by increasing rods lengths
     * and leave it in this state
     */
    public void sortByIncreasingRodsLength() {
        Collections.sort(rods);
    }

    /**
     * Sort the collection by decreasing rods lengths
     * and leave it in this state
     */
    public void sortByDecreasingRodsLength() {
        Collections.sort(rods, Collections.reverseOrder());
    }

    /**
     * List all rods in collections separated by commas
     * @return      string description of collection
     */
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (Rod part : rods) {
            out.append(part.toString());
            out.append(", ");
        }

        return out.substring(0, out.lastIndexOf(", "));
    }

    /**
     * Get the shortest rod from given rods collection in which will
     * fit in the specified rod. the length of the specified rod
     * will be bigger or equal then the length of returned rod
     * @param rod               specified rod
     * @param rodsCollection    collection to search in
     * @return                  shortest rod from collection in which
     *                          will fit in specified rod
     */
    public static Rod getShortestRodFromRodCollectionInWhichFitsRod(Rod rod, RodsCollection rodsCollection) {
        rodsCollection.sortByIncreasingRodsLength();

        for (int i = 0; i < rodsCollection.size(); i++) {
            if (rodsCollection.getRod(i).getLength() >= rod.getLength()) {
                return rodsCollection.getRod(i);
            }
        }

        throw new IllegalArgumentException(
                "There is no long enough rod in collection to satisfy the rod of length " + rod.getLength());
    }

    /**
     * Get the longest rod from collection
     * @param rodsCollection        search for longest rod
     * @return                      longest rod from collection
     */
    public static Rod getLongestRodFromCollection(RodsCollection rodsCollection) {
        rodsCollection.sortByDecreasingRodsLength();
        return rodsCollection.getRod(0);
    }
}
