package com.procyk.build.project;

import java.util.Objects;

/**
 * Rod with given length class which can be compared to
 * the other one by its length and hash coded also
 * based on its length
 */
public class Rod implements Comparable<Rod> {
    private long length;

    /**
     * Create new Rod of given length
     * @param length    of rod to be created
     */
    public Rod(long length) {
        this.length = length;
    }

    /**
     * Read the length of the rod
     * @return          length of rod
     */
    public long getLength() {
        return length;
    }

    /**
     * Cut the current rod by decreasing its length and creating the new one
     * @param length    length to be cut from the current rod. Must be smaller
     *                  or equal to current rod length
     * @return          new rod which is the cut part of the rod
     * @throws          IllegalArgumentException when the given length id too big
     */
    public Rod cut(long length) {
        if (length > this.length) {
            throw new IllegalArgumentException("Rod of length " + this.length +
                                               " is to short to cut from it a part of length " + length);
        }
        else {
            this.length -= length;
            return new Rod(length);
        }
    }

    /**
     * Compare two rods by their lengths
     * @param r     rod tobe compared
     * @return      standard comaprision result of lengths of this and r rods
     */
    @Override
    public int compareTo(Rod r) {
        return Long.compare(this.getLength(), r.getLength());
    }

    /**
     * Get the String description of Rod
     * @return      description of rod
     */
    @Override
    public String toString() {
        return  "Rod of length " + length + " mm";
    }

    /**
     * Hash the rod based on its length
     * @return      the hash code of rode
     */
    @Override
    public int hashCode() {
        return Objects.hash(length);
    }

    /**
     * Comapre rod with some object to check if they are equal.
     * Two rods are equal when they have the same lengths
     * @param obj       to be compared with rod
     * @return          true when the same, otherwise false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        else if ( ! (obj instanceof Rod)) {
            return false;
        }
        else {
            Rod r = (Rod) obj;
            return r.getLength() == this.getLength();
        }
    }
}
