package com.procyk.build.project;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Data structure to represent the price list of rods using HasMap
 * and give the user of price list only needed operations for reading
 * the data from price list
 * Rod has to have the hshCode function implemented to work with
 * HashMap structure
 */
public class PriceList {

    private final HashMap<Rod, Long> priceList;

    /**
     * Create new empty price list with rods
     * and their prices
     */
    public PriceList() {
        priceList = new HashMap<>();
    }

    /**
     * Add new rod to price list. When Rod exists in price list
     * the price is changed
     * @param r         rod to be added to price list
     * @param price     price of added rod
     */
    public void addRodWithPrice(Rod r, long price) {
        priceList.put(r, price);
    }

    /**
     * Get the price of given rod object which is
     * identified by its length
     * @param r         rod to get price of
     * @return          the price of rod r
     */
    public long getPriceOf(Rod r) {
        return priceList.get(r);
    }

    /**
     * Find the price of the rod of given length
     * by creating the new rod of given length and
     * checking its price in price list
     * @param length    length of rod to get price of
     * @return          the price of specified rod
     */
    public long getPriceOf(long length) {
        return priceList.get(new Rod(length));
    }

    /**
     * Get the number of different rods in price list
     * @return          number of positions in price list
     */
    public int getNumberOfPossibleItems() {
        return priceList.size();
    }

    /**
     * Generate an array of the lengths of rods
     * which are in price list
     * @return          array of lengths of rods in price list
     */
    public long[] toArrayOfPriceListRodsLengths() {
        long[] lengths = new long[getNumberOfPossibleItems()];

        int i = 0;
        for (Rod r : priceList.keySet()) {
            lengths[i] = r.getLength();
            i++;
        }

        return lengths;
    }

    /**
     * Get the RodsCollection of rods in the price list sorted
     * by increasing rods lengths
     * @return      sorted price list rods collection
     */
    public RodsCollection getAvailableRodsSortedIncreasing() {
        RodsCollection rods = new RodsCollection(new ArrayList<>(priceList.keySet()));
        rods.sortByIncreasingRodsLength();
        return rods;
    }

    /**
     * Create the description of te price list by returning
     * all possible to buy rods and their prices in String
     * @return      description of the price list
     */
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder("Price list entries: ");

        priceList.forEach(
                (rod, price) ->
                {
                    out.append(rod.toString());
                    out.append(" and price ");
                    out.append(price);
                    out.append(" PLN, ");
                }
        );

        return out.substring(0, Math.min(out.lastIndexOf(", "), out.length()));
    }
}
